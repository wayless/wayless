# Wayless

Wayless is going to be a capability-based microkernel, written in Rust and inspired
by SeL4. Currently only basic functionality has been implemented, including
interrupt handling and virtual memory mapping. No syscalls have been implemented yet.
The kernel will likely only be targeting x86_64.

## Prerequisites
* `xargo`
* `grub`
* `mtools`
* `xorriso` 
* `qemu` (may be called `qemu-system` on your machine)
* `nasm`
* `clang`

## Prerequisites for building seL4
* `ninja` (`ninja-build` on ubuntu and debian)
* `cmake`

### Python modules
* `setuptools`
* `sel4-deps`



## Usage

A `Makefile` is included to allow for easy building and running of Wayless.
A few useful targets are included:
* `make`: this just builds Wayless
* `make run`: this builds Wayless and then runs it in QEMU
* `make with-kvm`: this builds Wayless and then runs it in QEMU with kvm enabled
* `make debug`: this builds Wayless and runs it in QEMU with gdb connection enabled
* `make gdb`: this does the same as `make debug`, but it spins up a gdb instance 
and connects to QEMU

Currently QEMU is the only supported emulator, other emulators would potentially
work but they are untested. 
