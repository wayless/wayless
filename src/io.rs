//
//    wayless, an operating system built in Rust
//    Copyright (C) Waylon Cude 2019
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, under version 2 of the License
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

// This file contains logic that lets us print to the serial port.
// It includes both a print! and a println! macro

use core::fmt;
use core::fmt::Write;
pub fn _print(args: fmt::Arguments) {
    let mut serial = Serial {};
    let _ = fmt::write(&mut serial,args);
}
#[macro_export]
macro_rules! print {
    ($($arg:tt)*) => ($crate::io::_print(format_args!($($arg)*)));
}

#[macro_export]
macro_rules! println {
    ($fmt:expr) => (print!(concat!($fmt, "\n")));
    ($fmt:expr, $($arg:tt)*) => (print!(concat!($fmt, "\n"), $($arg)*));
}
struct Serial {}
impl Write for Serial {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        for c in s.chars() {
            unsafe {
            asm!("outb $1, $0"
                 :
                 : "{dx}" (0x3F8_u16), "{al}" (c as u8)
                 :
                 : "volatile" );}
        }
        Ok(())
    }
}
