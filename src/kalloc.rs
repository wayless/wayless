//
//    wayless, an operating system built in Rust
//    Copyright (C) Waylon Cude 2019
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, under version 2 of the License
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

use crate::{__KERNEL_END,KERNEL_BASE};
use crate::bootinfo::BootInfo;
use core::cmp;
use crate::stack::StaticStack;
use spin::Mutex;
use x86_64::{self,align_down};

// We make our allocator globally accessible
pub static ALLOC: Mutex<PhysAlloc> = Mutex::new(PhysAlloc::new());


// Our physical memory allocator consists of a bunch of stacks
// that contain varying sizes of chunks of memory. When we want to
// get a new 4KiB chunk of memory we check the pages stack first.
// If that's empty we pop off of the hexapages stack and divide
// it up into 16 pages, pushing those onto the pages stack.
// If that's empty we move up to the next stack, and so on. This
// lets us allocate up to 4 GiB of memory
#[derive(Debug)]
pub struct PhysAlloc {
    //256 in a mib
    pages: StaticStack<u64>,
    hexapages: StaticStack<u64>, //16 pages in an octopage
    mib_blocks: StaticStack<u64>, //16 hexapages in a mib_block
    hexamib_blocks: StaticStack<u64> //16 mib_blocks in a hexamib_block
}
impl PhysAlloc {
    pub const fn new() -> PhysAlloc {
        PhysAlloc {
            pages: StaticStack::new_const(0),
            hexapages: StaticStack::new_const(0),
            mib_blocks: StaticStack::new_const(0),
            hexamib_blocks: StaticStack::new_const(0),
        }
    }
    // We can free chunks of physical memory using this function.
    pub fn free(&mut self, addr: u64) {
        //Push the freed address back on the stack
        //MAYBE unmap it?
        self.pages.push(addr);
    }
    // This function allocates chunks of physical memory.
    // This works by checking the pages stack, then the hexapages stack,
    // and so on. If everything fails it just returns a None, signaling
    // that we are out of memory.
    pub fn alloc_page(&mut self) -> Option<u64> {
        if let Some(res) = self.pages.pop() {
            Some(res)
        } else if let Some(res) = self.hexapages.pop() {
            //TODO: move this logic into indiviual functions
            for i in 1 .. 16 {
                self.pages.push(res+ i*4096);
            }
            Some(res)
        } else if let Some(res) = self.mib_blocks.pop() {
            for i in 1 .. 16 {
                self.hexapages.push(res+ i*16*4096);
            }
            for i in 1 .. 16 {
                self.pages.push(res+ i*4096);
            }
            Some(res)
        } else if let Some(res) = self.hexamib_blocks.pop() {
            for i in 1 .. 16 {
                self.mib_blocks.push(res+ i*1024*1024);
            }
            for i in 1 .. 16 {
                self.hexapages.push(res+ i*16*4096);
            }
            for i in 1 .. 16 {
                self.pages.push(res+ i*4096);
            }
            Some(res)

        } else {
            None
        }
    }
    // This function adds a new chunk of physical memory to the allocator.
    // The start and end are specified, and everything between that is
    // added.
    pub fn add_memory(&mut self, start: u64, end: u64) {
        //Round down to nearest page alignment, then increase value by a page
        //so it is in the range
        let mut start = x86_64::align_up(start,1<<12) as i64;
        let end: i64 = end as i64;


        //TODO: type conversion might be bad

        //While there's enough space to allocate a page
        while end - start >= 4096 {
            if end - start >= 1024*1024*16 {
                self.hexamib_blocks.push(start as u64);
                start += 1024*1024*16;
            } else if end - start >= 1024*1024 {
                self.mib_blocks.push(start as u64);
                start += 1024*1024;
            } else if end - start >= 16*4096 {
                self.hexapages.push(start as u64);
                start += 16*4096;
            } else {
                self.pages.push(start as u64);
                start += 4096;
            }

        }
    }

}

// This is a global function that gives us a new page, it's designed
// to make allocating pages easier.
// TODO: This really shouldn't unwrap, we should actually handle the system
// being out of memory
pub fn alloc_page() -> u64 {
    ALLOC.lock().alloc_page().unwrap()
}

// This is a global function that makes adding a new chunk of memory
// easier
pub fn add_memory(start: u64, end: u64) {
    println!("ADDING MEM: {:x} to {:x}",start,end);
    ALLOC.lock().add_memory(start,end)
}
// This function takes in a bootinfo struct, finds all of the bootinfo
// tags that point us to memory, and adds all of the corresponding
// memory into our allocator.
pub fn init(bootinfo: &BootInfo) -> Result<u64,AllocError>{

    let kernel_end = unsafe{(&__KERNEL_END as *const u32) as u64}
                        - KERNEL_BASE;

    //Keep track of how much memory we've added so we don't allocate stuff multiple
    //times
    let mut current_added = 0u64;

    //Add memory up to the lowest memory hole using the bootinfo
    //basic memory tag
    if let Some((_lower,higher)) = bootinfo.get_basic_memory() {
        //First 2 MiB are reserved for kernel code
        add_memory(kernel_end,align_down((higher+1024*1024) as u64,0x1000));
        current_added = cmp::max(kernel_end,align_down((higher+1024*1024) as u64,0x1000));
    } 

    if let Some((version,entries)) = bootinfo.get_memory_map() {
        //If it's the right version
        if version == 0 {
            for entry in entries {
                let entry_end = entry.base_addr + entry.length;
                //If it's usable RAM
                if entry.ty == 1 && entry_end > current_added {
                    let entry_start = if entry.base_addr >= current_added {
                        entry.base_addr
                    } else {
                        current_added
                    };
                    add_memory(entry_start,entry_end);
                    current_added = entry_end;
                }
            }
        } else {
            return Err(AllocError::WrongMemoryMapVersion);
        }
    } else {
        return Err(AllocError::NoMemoryMap);
    }
    if current_added == 0 {
        Err(AllocError::NoMemory)
    } else {
        Ok(current_added)
    }
}
//Maybe todo: think up good names for these?
#[derive(Debug,Clone)]
pub enum AllocError {
    NoMemory,
    NoMemoryMap,
    WrongMemoryMapVersion

}
