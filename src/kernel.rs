//
//    wayless, an operating system built in Rust
//    Copyright (C) Waylon Cude 2019
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, under version 2 of the License
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#![no_std]
#![feature(lang_items)]
#![feature(asm)]
//#![feature(panic_implementation)]
#![feature(abi_x86_interrupt)]
#![feature(const_fn)]
#![feature(alloc_error_handler)]

//Ignore warnings when running tests
#![cfg_attr(test, allow(dead_code, unused_macros, unused_imports))]


//And include the standard library
#[cfg(test)]
#[macro_use]
extern crate std;

#[cfg(test)]
#[macro_use]
extern crate proptest;

//extern crate compiler_builtins;
extern crate rlibc;

extern crate x86_64;
extern crate x86;

extern crate spin;

#[cfg(not(test))]
#[macro_use]
extern crate alloc;

//Don't override the print macro from std
//if we're running tests
#[cfg_attr(not(test), macro_use)]
mod io;

mod bootinfo;
mod capability;
mod error;
mod kalloc;
mod interrupts;
//mod invocation;
mod memory;
mod stack;
#[cfg(not(test))]
#[allow(unused)]
mod std;
mod syscall;
mod thread;
//Don't override panic! when testing
#[cfg(not(test))]
pub mod unwind;

//Create an extra module for bindings to reduce name conflicts
//mod sel4 {
//    #![allow(non_upper_case_globals)]
//    #![allow(non_camel_case_types)]
//    #![allow(non_snake_case)]
//    #![allow(unused)]
//    include!("../build/bindings.rs");
//}
use wayless_api::invocation;

use slab_allocator::LockedHeap;

#[cfg_attr(not(test),global_allocator)]
static ALLOCATOR: LockedHeap = LockedHeap::empty();

use crate::bootinfo::{RawBootInfo,BootInfo};


extern "C" {
    fn halt() -> !;
    fn init_timer();
}
extern {
    static __KERNEL_START: u32;
    static __KERNEL_END: u32;

    //We just need addresses to these
    //#[no_mangle]
    //static heap_bottom: u64;
    //#[no_mangle]
    //static heap_top: u64;
}
const KERNEL_BASE: u64 = 0xFFFF800000000000;


//Once we're here the kernel is in the higher half
#[no_mangle]
pub fn kmain(bootinfo: *mut RawBootInfo) {
    println!("Entered kmain");
    println!("BOOTINFO: {:p}",bootinfo);
    let mut bootinfo = BootInfo::new(bootinfo); //Read bootinfo into rust struct
    println!("Read in bootinfo, got {} tags",bootinfo.multiboot_size);

    //Check to make sure the bootinfo magic number is correct
    if bootinfo.bad_magic() {
        println!("Bad bootinfo magic value: {}", bootinfo.magic);
        return
    } else {
        println!("Read good bootinfo magic");
    }

    //Initialize interrupts early so we get extra debugging info
    bootinfo.initialize_interrupts();
    println!("Initialized interrupts");

    println!("Initializing memory");
    match kalloc::init(&bootinfo) {
        Ok(size) => {
            print!("Successfully added memory up to ");
            if size >= 1<<30 { println!("{:.2} GiB",(size as f64)/(1<<30) as f64) }
            else if size >= 1<<20 { println!("{:.2} MiB",(size as f64)/(1<<20) as f64) }
            else if size >= 1<<10 { println!("{:.2} KiB",(size as f64)/(1<<10) as f64) }
            else { println!("{} bytes",size) }

            //NOTE: This is probably happening in structures.asm now
            //Map all of the physical memory to itself + kernel_base
            //memory::identity_map(size);
        },
        Err(e) => println!("Failed to add memory: {:?}",e)
    }


    let kernel_start = unsafe{(&__KERNEL_START as *const u32) as u64};
    let kernel_end = unsafe{(&__KERNEL_END as *const u32) as u64};
    println!("KERNEL STARTS AT 0x{:x}",kernel_start-KERNEL_BASE);
    println!("KERNEL ENDS AT 0x{:x}",kernel_end-KERNEL_BASE);
    println!("KERNEL SIZE 0x{:x}",kernel_end-kernel_start);


    println!("Testing writing to allocated page");
    memory::kmap(0x50000000,kalloc::alloc_page(),0b1111);
    println!("Mapped memory");
    let value = 0x50000000 as *mut u64; 
    unsafe {
        *value = 0xFADE1137;
        if *value == 0xFADE1137 {
            println!("Got correct value back!");
        } else {
            println!("ERROR: Got incorrect value back!");
        }
    }


    println!("Testing writing to unallocated page");
    let value = 0x40000000 as *mut u64;
    unsafe {
        *value = 0xEA4478FA;
        if *value == 0xEA4478FA {
            println!("Got correct value back!");
        } else {
            println!("ERROR: Got incorrect value back!");
        }
    }


    println!("Initializing syscalls");
    syscall::init();

    println!("Initializing starting heap");

    //Kernel heap is 16 MiB at most
    let heap_size = 16*1024*1024;
    //let heap_bottom_ptr =  unsafe{(heap_bottom as *const u64) as u64};
    //let heap_top_ptr =  unsafe{(&heap_top as *const u64) as u64};
    
    //Set up an initial heap so we can use alloc
    //let heap_bottom_ptr = unsafe {heap_bottom as *mut ()};
    //let heap_bottom: u64 = KERNEL_BASE + (0xC << 40);
    let heap_bottom: u64 = 0xC << 40;

    println!("HEAP BOTTOM: 0x{:x?}",heap_bottom);
    //println!("HEAP TOP: 0x{:x?}",heap_top_ptr);
    println!("HEAP SIZE: 0x{:x?}",heap_size);

    // Initialize our main allocator library.
    // NOTE: we might be able to get more space because
    // we have a page fault handler
    //let heap_size = heap_top_ptr as u64 - heap_bottom_ptr as u64;
	unsafe {
        ALLOCATOR.init(heap_bottom as usize, heap_size as usize);
    }

    let heap_test = vec![1,2,3];
    let sum: isize = heap_test.iter().sum();
    if sum == 6 {
        println!("Heap working!");
    } else {
        println!("Heap broken, got {} back when we should have 6",sum);
    }

    //Here we include the userspace program, which we can then load
    //NOTE: I don't think I can make this a constant
    //so you have to change the definition here if you want to change
    //what userspace program gets ran
    let userspace = include_bytes!("../userspace/start");
    let parsed_prog = match goblin::elf::Elf::parse(userspace){
        Ok(k) => k,
        Err(e) => panic!("Got error when parsing userspace program: {:?}",e),

    };

    let pdp = &unsafe{(*bootinfo.pml4)[256]};
    //Initialize threads
    thread::init_threads();
    //Add kernel thread
    thread::add_kernel_thread();
    //And then user thread
    let _result = thread::load_user_binary(parsed_prog, userspace, pdp);

    //Start the APIC timer
    println!("Initializing APIC timer");
    unsafe {
        init_timer();
    }
    println!("Initialized timer");

    //Go into a loop
    let mut x = 0;
    let mut y = 0;
    loop {
        y+=1;
        //println!("Y: {}",y);
        if y >= 100000 {
            x+=1;
            y = 0;
            println!("X: {}",x);
        }
        //Context switch for a bit then get out
        if x >= 5 { break };
    }

    //This will stay commented out until the syscall handler is working
    //unsafe {asm!("mov rbx, rsp
    //             mov rdx, -1
    //             syscall
    //             mov rsp,rbx" : : : "rbx" : "intel")};


}
