//
//    wayless, an operating system built in Rust
//    Copyright (C) Waylon Cude 2019
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, under version 2 of the License
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

//This conditional lets us use less imports to make both normal compilation
//and testing work
#[cfg(not(test))]
use alloc as std;

mod ioport;

use std::sync::Arc;
use std::vec::Vec;

use spin::RwLock;
use crate::invocation::InvocationLabel;

use ioport::handle_io_invocation;

use crate::error::Error;

// A LockedCap is a spinlock around a capability. It lets us work
// more easily with locks
#[derive(Clone,Debug)]
pub struct LockedCap(Arc<RwLock<Capability>>);
impl LockedCap {
	// Returns a RwLockGuard which gives read access to the underlying Cap
    pub fn read(&self) -> spin::RwLockReadGuard<'_, Capability, > {
        self.0.read()
    }
	// Returns a RwLockGuard which gives write access to the underlying Cap
    pub fn write(&self) -> spin::RwLockWriteGuard<'_, Capability, > {
        self.0.write()
    }
    // Creates a new LockedCap from a Capability
	pub fn new(cap: Capability) -> LockedCap {
		LockedCap(Arc::new(RwLock::new(cap)))
	}
    // This function will try to get the child of the given cap.
    pub fn get_cap(&self, address: u64, depth_limit: u8) -> Result<LockedCap,Error> {
        //We've traversed to the final node, so return it
        if depth_limit == 0 {
            Ok(self.clone())
        } else {
            self.0.read().get_cap(address,depth_limit)
        }
        
    }
    pub fn send(&self, label: u64, length: u64, registers: &mut [u64; 4]) {
        self.write().send(label, length, registers);
    }
    pub fn is_endpoint(&self) -> bool {
        self.read().is_endpoint()
    }
}

impl PartialEq for LockedCap {
    fn eq(&self, other: &Self) -> bool {
        *self.read() == *other.read()
    }
}

// This enum holds all possible capability types. There's only three
// implemented, but it should be easy to fill this out with more 
// capabilities.
#[derive(Clone,Debug,PartialEq)]
pub enum Capability {
    CNode(CNode),
    //Start and end of range
    IOPort(u64,u64),
    //Start and end of physical memory range, along with a counter
    Untyped(u64,u64,u64)
}
impl Capability {
    pub fn get_cap(&self, address: u64, depth: u8) -> Result<LockedCap,Error> {
        match self {
            Capability::CNode(ref node) => node.get_cap(address, depth),
            //TODO: Should this always be an error?
            _ => Err(Error::LookupFailed)
        }
    }
    // This function handles a `send` to a capability. The only implemented
    // capability is an IOPort, so that's the only thing we handle.
    // TODO: This function needs a lot more work
    pub fn send(&mut self, label: u64, length: u64, registers: &mut [u64; 4]) -> Result<(),Error> {
        if let Some(label) = InvocationLabel::from_u32(label as u32) {
            match self {
                Capability::IOPort(start,end) => {
                    handle_io_invocation(label,length,registers,*start,*end)
                }
                _ => {
                    // TODO: Is this the right error to return?
                    return Err(Error::IllegalOperation)
                }
            }
        } else {
            //TODO: handle invalid labels
            unimplemented!();
        }
    }
    // We check to see if the capability is an endpoint or not. This
    // lets us know whether we can keep recursing into it to find a
    // child capability.
    pub fn is_endpoint(&self) -> bool {
        match self {
            Capability::CNode(_) => false,
            _ => true
        }
    }
}
//This struct represents a CNode
#[derive(Clone,Debug,PartialEq)]
pub struct CNode {
    guard: u64,
    guard_size: u8,
    radix: u8,
    // TODO: This probably shouldn't be a Vec, because you can probably
    // get the system to run out of memory by making the Vec grow too much
    children: Vec<Option<LockedCap>>,
}

impl CNode {
    // How many entries the cnode can hold
    pub fn size(&self) -> usize {
        //self.children.len()
        1 << self.radix
    }   
    // This function adds a new capapability into the children of
    // the current CNode
    pub fn add_child(&mut self, cap: LockedCap, index: u64) {
        //TODO: bounds check
        self.children[index as usize] = Some(cap);
    }
    // This creates a new CNode with space for 2^radix children
    pub fn new(radix: u8) -> CNode{
        let mut children = Vec::with_capacity(1<<radix);
        //TODO: does this need to be a loop?
        for _ in 0 .. 1<<radix {
            children.push(None);
        }
        CNode {
            guard: 0,
            guard_size: 0,
            radix: radix,
            children: children
        }
    }
    // This sets the guard of the current CNode. When addressing the CNode
    // you need to stick the guard in there or addressing will fail.
    pub fn set_guard(&mut self, guard: u64, guard_size: u8) {
        self.guard = guard;
        self.guard_size = guard_size;
    }
    // This function takes and address and returns the corresponding
    // capability, or an error.
    pub fn get_cap(&self, address: u64, depth_limit: u8) -> Result<LockedCap,Error> {
        let total_size = self.guard_size + self.radix;
        if depth_limit < total_size {
            Err(Error::LookupFailed)
        } else {
            let given_guard = if self.guard_size != 0 {
                //Select the guard
                address >> (64 - self.guard_size)
            } else {
                0
            };
            //A mask of radix bits
            let addr_mask = (1 << self.radix) - 1; 
            //Select the address
            let given_address = address >> (64 - total_size) & addr_mask;
            if given_guard != self.guard {
                Err(Error::LookupFailed)
            } else {
                let child = self.children.get(given_address as usize)
                             .cloned()
                             //Unwrap first layer from get
                             .ok_or(Error::LookupFailed)?
                             //And another from the inside of the Vec
                             .ok_or(Error::LookupFailed)?;
                if child.is_endpoint() {
                    Ok(child)
                } else {
                    //Recursive call after we get rid
                    //of some of the leading bits
                    child.get_cap(address << total_size,
                                  depth_limit - total_size)
                }
            }
        }
    }


}
#[cfg(test)]
mod test {
    use super::{Capability,CNode,LockedCap};
    use crate::error::Error;
    #[test]
    fn address_cap() {
        let new_cap = Capability::Untyped(0,5,1);
        let locked = LockedCap::new(new_cap.clone());
        //Make a CNode with space for 16 elements
        let mut cnode = CNode::new(4);
        cnode.add_child(locked,12);
        let locked_node = LockedCap::new(Capability::CNode(cnode));
        assert_eq!(new_cap,
                   *locked_node.get_cap(0xC000 << 48,64).unwrap().read());

    }
    #[test]
    fn invalid_address_cap() {
        //Make a CNode with space for 16 elements
        let cnode = CNode::new(4);
        let locked_node = LockedCap::new(Capability::CNode(cnode));
        assert_eq!(Err(Error::LookupFailed),
                   locked_node.get_cap(0xC000 << 48,64));

    }
    #[test]
    fn guard_address_cap() {
        let new_cap = Capability::Untyped(0,5,1);
        let locked = LockedCap::new(new_cap.clone());
        //Make a CNode with space for 16 elements
        let mut cnode = CNode::new(4);
        cnode.set_guard(0xDEADBEEF,32);
        cnode.add_child(locked,12);
        let locked_node = LockedCap::new(Capability::CNode(cnode));
        assert_eq!(new_cap,
                   *locked_node.get_cap(0xDEADBEEFC000 << 16,64).unwrap().read());

    }
    #[test]
    fn invalid_guard() {
        let new_cap = Capability::Untyped(0,5,1);
        let locked = LockedCap::new(new_cap.clone());
        //Make a CNode with space for 16 elements
        let mut cnode = CNode::new(4);
        cnode.set_guard(0xFFFFFFFF,32);
        cnode.add_child(locked,12);
        let locked_node = LockedCap::new(Capability::CNode(cnode));
        assert_eq!(Err(Error::LookupFailed),
                   locked_node.get_cap(0xDEADBEEFC000 << 16,64));

    }
}
