//
//    wayless, an operating system built in Rust
//    Copyright (C) Waylon Cude 2019
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, under version 2 of the License
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

use core::fmt;
use core::ops::{Index,IndexMut};
use crate::kalloc;
use crate::KERNEL_BASE;
use x86_64::align_up;

//Turn everything on and enable caching
const DEFAULT_FLAGS: u16 = 0b00001111;

//Generic page table entry
#[repr(C)]
#[derive(Clone,Copy,PartialEq)]
pub struct PTE(pub u64);

//Align to page
#[repr(C)]
#[repr(align(4096))]
//A page table is just an array of page table entries
pub struct PageTable(pub [PTE; 512]);

impl PageTable {
    // This zeroes a page table out so there isn't garbage in there
    pub fn reset(&mut self) {
        self.0 = [PTE(0);512] 
    }
}

//This lets us index a page table for easier use
impl IndexMut<usize> for PageTable {
    fn index_mut<'a>(&'a mut self, index: usize) -> &'a mut PTE {
        &mut (self.0)[index]
    }
}
impl Index<usize> for PageTable {
    type Output = PTE;
    fn index<'a>(&'a self, index: usize) -> &'a PTE {
        &(self.0)[index]
    }
}


impl PTE {
    pub fn offset(&mut self, index: usize) -> &mut PTE {
        //TODO: check flags and then alloc a new pagetable if stuff isn't present
        let flags = self.0 & 0xFFF;
        let present = flags & 0x1;


        //If the page table doesn't exist yet allocate a new one
        if present == 0 {
            //Allocate a new table and turn the address into an int
            let addr = kalloc::alloc_page();

            let t = unsafe {&mut *((addr + KERNEL_BASE) as *mut PageTable)};

            //Zero the table
            for entry in t.0.iter_mut() {
                *entry = PTE(0);
            }
            //Set the flags
            let entry = addr | DEFAULT_FLAGS as u64;
            // And set the current entry with a valid address
            self.0 = entry;
        }

        //Now the address is guaranteed to exist

        //Get all except the last 12 bits
        let table_address = self.0 & (::core::u64::MAX ^ 0xFFF);
        
        //We add the base here so we access the table from
        //the higher half of memory
        let table_address = table_address + KERNEL_BASE;

        let table = unsafe {
            ::core::slice::from_raw_parts_mut(table_address as *mut PTE,512)
        };

        &mut table[index]
    }
    pub fn set_phys(&mut self, phys: u64, flags: u16) {
        //Cap flags at 12 bits
        let flags = flags & 0xFFF;

        //Allocate a new table and turn the address into an int
        //let addr = kalloc(4*1024, 4*1024) as u64;
        //Set the flags
        let entry = phys | flags as u64;
        // And set the current entry with a valid address
        //
        self.0 = entry;
    }
}

// This lets us print out the contents of a page table entry, for easier
// debugging
impl fmt::Debug for PTE {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.0 > 0xFFF {
            let table = (self.0 & !0xFFF) as *const PageTable;
            unsafe {
                for i in (*table).0.iter() {
                    write!(f,"{:x},",i.0)?;
                }
            }
        }
        Ok(())
    }

}

// kmap maps a virtual address to some backing physical memory
pub fn kmap(virt: u64, phys: u64, flags: u16) {
    //The pml4 address is stored in cr3, so we have to retrieve that
    //and turn it into an array
    // v be wary of segfaults
    let pml_addr: u64; //unsafe { *PML4_ADDRESS } ;
    unsafe {asm!("mov %cr3, %rax" : "={rax}"(pml_addr))};
    let pml_addr = pml_addr | KERNEL_BASE;
    let pml = unsafe {
        ::core::slice::from_raw_parts_mut(pml_addr as *mut PTE,512)
    };

    kmap_pml4(pml,virt,phys,flags);

    //Flush the tlb so the mapping takes effect
    flush_tlb();
}
pub fn kmap_pml4(pml: &mut [PTE], virt: u64, phys: u64, flags: u16) {

    //Get all of the offsets, they're all 9 bits
    let pml_offset = (virt >> 39) & 0x1FF;
    let offsets = [ (virt >> 30) & 0x1FF, //pdp offset
                    (virt >> 21) & 0x1FF, //pd offset
                    (virt >> 12) & 0x1FF];//pt offset

    //All offsets are guaranteed to fit in a usize
    let pte = &mut pml[pml_offset as usize]
        //Rust doesn't like this as a for loop because of borrowing stuff
        .offset(offsets[0] as usize)
        .offset(offsets[1] as usize)
        .offset(offsets[2] as usize);

    pte.set_phys(phys,flags);
}

// This forces the page table to be reloaded after we've added a page
fn flush_tlb() {
    unsafe {
        asm!("	mov	%cr3,%rax
                mov	%rax,%cr3" : : : "rax")
    }
}

//This function maps memory at address x to x + KERNEL_BASE
//We can start at 512 as we've already mapped that memory from asm
pub fn identity_map(end: u64) {
    for i in 512 .. align_up(end,0x1000)/0x1000 {
        let flags = 0b100001011;
        kmap(i*0x1000+KERNEL_BASE,i*0x1000,flags);
    }
}
