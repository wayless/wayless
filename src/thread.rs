//
//    wayless, an operating system built in Rust
//    Copyright (C) Waylon Cude 2019
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, under version 2 of the License
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

//use heapless::spsc::Queue;
//use heapless::consts::*;
#[cfg(not(test))]
use alloc as std;

//Maybe TODO: using something on the heap might be a bad idea
use std::collections::VecDeque;


use spin::{Mutex,Once};

// This is our thread queue, it's behind a Mutex lock so we don't
// try to access it from two threads at the same time.
pub static THREADS: Once<Mutex<ThreadQueue>> = Once::new();

//This macro makes getting threads easier
macro_rules! threads {
    () => {{
        THREADS.wait().unwrap().lock()
    }}
}

use goblin::elf::Elf;
use crate::memory::{PTE,PageTable,kmap_pml4};
use crate::kalloc::alloc_page;
use x86_64::{align_up,align_down};
use core::ptr;
use crate::KERNEL_BASE;
use crate::capability::{LockedCap,Capability,CNode};



//NOTE: Context has all the same functionality as registers so I'm
//fairly certain that it's unnecessary
//We force this to repr(C) so we can use this type
//via assembly
//
//We create one of these to save client registers during syscalls
//#[repr(C)]
//pub struct Registers {
//    //NOTE: These go in a weird, nonstandard order
//    //which you can only figure out by looking at
//    // src/arch/x86-64/thread.mac
//    regs: [u64; 18]
//}
//impl Registers {
//    pub fn new() -> Registers {
//        Registers {
//            regs: [0; 18]
//        }
//        
//    }
//}

// Context contains all of the saved state associated with a thread,
// including all of the registers (though I still need to save floating
// point registers)
//TODO: save floating point state in context
//Order of regs:
//rip
//rsp
//rflags
//rax
//rbx
//rcx
//rdx
//rsi
//rdi
//rbp
//r8
//r9
//r10
//r11
//r12
//r13
//r14
//r15
//tls_base
#[derive(Clone,Debug,PartialEq)]
#[repr(C)]
pub struct Context {
    pub regs: [u64; 19]
}
impl Context {
    pub fn new() -> Context {
        Context {
            regs: [0; 19]
        }
    }
}

#[derive(Clone,Debug,PartialEq)]
pub struct Thread {
    context: Context,
    segments: [u16; 6],
    //Pointer to the threads pml4
    vspace: u64,
    //Pointer to the threads cspace root
    cspace: Option<LockedCap>,
    //Whether we should run this thread or not
    active: bool,
    //Keep track of the priveleges for this thread
    kernel: bool,
    //A cptr to the reply capability
    reply: Option<LockedCap>
}
impl Thread {
    pub fn new() -> Thread {
        Thread {
            context: Context::new(),
            //Set up a user thread by default
            segments: [0x23,0x1B,0x1B,0x1B,0x1B,0x1B],
            //These are invalid values until the user sets them
            // via configure
            vspace: 0,
            cspace: None,
            //The new thread is not active
            active: false,
            kernel: false,
            reply: None,
        }
    }
    // This function takes an array of registers and writes
    // them into the given thread
    pub fn write_registers(&mut self, new_regs: &[u64]) {
        for (i,r) in new_regs.iter().enumerate() {
            self.context.regs[i] = *r;
        }
    }
    // This function copies the segments given, and sticks them
    // into our thread
    pub fn save_segments(&mut self, segments: &[u16;6]) {
        self.segments = *segments;
    }
    // This sets the cr3 of our thread, the pointer to the root page table
    pub fn set_vspace(&mut self, addr: u64) {
        self.vspace = addr;
    }
    // This gets the cr3
    pub fn get_vspace(&self) -> u64 {
        self.vspace
    }
    // This sets the root capability of the current thread
    pub fn set_root(&mut self, root: LockedCap) {
        self.cspace = Some(root);
    }
    // And this gets that root capability
    pub fn get_root(&self) -> Option<LockedCap> {
        self.cspace.clone()
    }
    // This function retrieves the segments from the current thread
    pub fn get_segments(&self) -> [u16;6] {
        self.segments
    }
    // We can enable or disable the thread via these functions
    pub fn enable(&mut self) {
        self.active = true;
    }
    pub fn disable(&mut self) {
        self.active = false;
    }
    // We can set the correct segments for either a user or kernel thread,
    // allowing us to make either type
    pub fn set_kernel_thread(&mut self) {
        //Set up the segments for a kernel thread
        self.segments = [0x08,0x10,0x10,0x10,0x10,0x10];
    }
    pub fn set_user_thread(&mut self) {
        //Set up the segments for a user thread
        self.segments = [0x23,0x1B,0x1B,0x1B,0x1B,0x1B]
    }
    // And we can get the context from a thread
    pub fn get_context(&self) -> &Context {
        &self.context
    }
    // Get the reply capability and remove it from this thread,
    // as each reply cap can only be used once
    pub fn get_reply_cap(&mut self) -> Option<LockedCap> {
        self.reply.take()
    }

}

//This is a round-robin queue of threads
pub struct ThreadQueue {
    //Store 512 threads
    //queue: Queue<Thread, U512>
    queue: VecDeque<Thread>
}
impl ThreadQueue {
    pub fn new() -> ThreadQueue {
        ThreadQueue {
            queue: VecDeque::new()
        }
    }
    pub fn enqueue(&mut self, new_thread: Thread) {
        self.queue.push_back(new_thread)
    }
    pub fn dequeue(&mut self) -> Option<Thread> {
        self.queue.pop_front()
    }
    pub fn peek(&self) -> Option<&Thread> {
        self.queue.front()
        //match self.queue.iter().next() {
        //    Some(t) => Some(t.clone()),
        //    None => None
        //}
    }
    pub fn replace_front(&mut self, to_add: Thread) {
        let _ = self.queue.pop_front();
        self.queue.push_front(to_add);
    }
}

// These global functions let us add threads easier
pub fn add_thread(new_thread: Thread)  {
    threads!().enqueue(new_thread)       
}
pub fn init_threads()  {
    THREADS.call_once(|| Mutex::new(ThreadQueue::new()));
}
pub fn current_thread() -> Option<Thread> {
    match threads!().peek() {
        //TODO: is it better to have this clone
        // or to keep the lock active?
        Some(s) => Some(s.clone()),
        _ => None
    }
}
pub fn replace_thread(to_add: Thread) {
    threads!().replace_front(to_add);
}
// This switches to the next thread. It is called whenever we yield
// or when something gets preempted and a thread switch happens.
#[no_mangle]
pub fn switch_thread(context: *mut Context, segments: *mut [u16;6]) -> u64 {
    let mut threads = threads!();
    let current_thread = threads.dequeue();
    if let Some(mut thread) = current_thread {
        //Deref raw pointer
        let context = unsafe {&mut *context}; 
        let segments = unsafe {&mut *segments}; 
        //let segments = unsafe {&mut *segments}; 
        //Save the registers
        thread.write_registers(&context.regs);
        //This can probably be optimized out, maybe check if thread
        //segments are equal to current segments
        thread.save_segments(&segments);
        // user thread by default so we don't need this:
        // else {
        //    thread.set_user_thread();
        //}
        //Add our thread to the end of the thread list
        let _result = threads.enqueue(thread);
        //TODO: don't unwrap here
        let new_thread = threads.peek().unwrap();
        *context = new_thread.get_context().clone();
        *segments = new_thread.get_segments();

        return new_thread.get_vspace();

    }
    return 0;
}
// This creates a new kernel thread, which is only used so we can context
// switch back into the kernel once we've enabled thread stuff
pub fn add_kernel_thread() {
    let mut new_thread = Thread::new();
	let pml_addr: u64;
    unsafe {asm!("mov %cr3, %rax" : "={rax}"(pml_addr))};
    new_thread.set_vspace(pml_addr);
    new_thread.enable();
    //new_thread.set_kernel_thread();
    //This shouldn't fail
    add_thread(new_thread);
}
// This loads a user binary into a new address space, and creates a
// corresponding thread. It's how the first userspace thread is created.
pub fn load_user_binary(header: Elf, bytes: &[u8], kernel_pdp: &PTE) -> Result<(),()>{
    let user_pml4 = alloc_page();
    let pml4_ptr = unsafe {&mut *((user_pml4 + KERNEL_BASE) as *mut PageTable)};
    //Clear the page table
    pml4_ptr.reset();

    //Load program into new address space
    for prog_header in header.program_headers {
        //Check if it's a loadable segment
        if prog_header.p_type == 1 {
            //Find how many pages we need
            let mem_size = prog_header.p_memsz;
            let file_size = prog_header.p_filesz;

            let mut pages = vec![];
            //Get number of pages
            //huh, I'm assuming the address is aligned
            let mem_start = align_down(prog_header.p_vaddr,4096);
            let mem_end = align_up(prog_header.p_vaddr + mem_size,4096);
            let num_pages = (mem_end - mem_start)/4096;
            let mut mem_cursor = mem_start;
            for _ in 0 .. num_pages {
                //Allocate pages for our new program
                //And keep track of what the vaddr is
                pages.push((mem_cursor,alloc_page()));
                mem_cursor += 4096;
            }
            //copy over the bytes
            let mut offset = prog_header.p_offset;
            let end = offset + file_size;
            //Use offset as a cursor and continuously copy over bytes
            //We also keep track of what page we're at
            let mut page_index = 0;
            while offset < end {
                let to_read = end - offset;
                //If we have a whole page to copy over
                if to_read >= 4096 {
                    unsafe {
                    ptr::copy_nonoverlapping(
                        bytes.as_ptr().offset(offset as isize),
                        (pages[page_index].1 + KERNEL_BASE) as *mut u8, 
                        4096);
                    }
                    offset += 4096;
                    page_index += 1;
                } else {
                    unsafe {
                    ptr::copy_nonoverlapping(
                        bytes.as_ptr().offset(offset as isize),
                        //We offset by the kernel base to copy
                        //stuff to the right physical address
                        (pages[page_index].1 + KERNEL_BASE) as *mut u8, 
                        to_read as usize);
                    }
                    offset += to_read;
                    page_index += 1;
                }
            }

            //and finally set up the new pml4
            //and map the pages we need
            for (vaddr,paddr) in pages {
                //MAYBE TODO: potentially make page read-only
                kmap_pml4(&mut pml4_ptr.0, vaddr, paddr, 0b111);   
            }


        }
    }
    //Map kernel into higher half
    pml4_ptr[256] = *kernel_pdp; 

    let mut new_thread = Thread::new();
    let regs: [u64; 3] = [ header.entry,0,1<<9 ]; //Set RIP and RFLAGS
    new_thread.write_registers(&regs);
    new_thread.set_vspace(user_pml4);
    let io_cap = LockedCap::new(Capability::IOPort(0,65535));
    //Our new cnode has space for two children
    let mut new_cnode = CNode::new(1);
    //Add an io cap at index 0
    new_cnode.add_child(io_cap,0);
    //Create the root cap
    let root_cap = LockedCap::new(Capability::CNode(new_cnode));
    new_thread.set_root(root_cap);
    new_thread.enable();
    //We don't actually want to make it a kernel thread, this is just for testing
    //new_thread.set_kernel_thread();
    //This shouldn't fail
    add_thread(new_thread);
    Ok(())

}

#[cfg(test)]
mod test {
    use std;

    use super::{THREADS,Thread,ThreadQueue,init_threads};
    use super::{add_thread,current_thread,switch_thread};
    use super::Context;

    #[test]
    fn add_one_thread() {
        let mut threads = ThreadQueue::new();
        let mut thread = Thread::new();
        thread.set_vspace(0xDEADBEEF);
        threads.enqueue(thread.clone());
        assert_eq!(Some(thread),threads.dequeue());
    }
    //This test tries adding a bunch of threads and then switching
    #[test]
    fn many_thread_switch() {
        let num = 13;
        let switches = 317;
        let mut context = Context::new();
        let mut segments = [0; 6];
        //This is the only test we can use global threads :''')
        init_threads();
        //Add a bunch of threads, with vspace = thread number
        for i in 0 .. num {
            let mut t = Thread::new();
            t.set_vspace(i);
            add_thread(t);
        }
        //Switch threads a bunch of times
        for _ in 0 .. switches {
            switch_thread(&mut context, &mut segments);
        }
        //Get the current thread
        let t = current_thread().unwrap();
        //And its vspace, which corresponds to a thread number
        let thread_num = t.get_vspace();
        //The final thread number should be switches modulo num
        assert_eq!(thread_num, switches % num);
    }
}
