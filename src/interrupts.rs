//
//    wayless, an operating system built in Rust
//    Copyright (C) Waylon Cude 2019
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, under version 2 of the License
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

use crate::memory::kmap;
use crate::kalloc::alloc_page;
use x86_64;
use x86_64::structures::idt::{InterruptDescriptorTable,InterruptStackFrame,PageFaultErrorCode};

pub type InterruptTable = InterruptDescriptorTable;

use crate::KERNEL_BASE;
use core::mem;

static APIC_BASE: u64 = KERNEL_BASE + 0xFEE00000;

extern "C" {
    fn context_switch();
}

pub fn initialize(idt: &mut InterruptTable) {
    macro_rules! set_handler {
        ($func:ident) => {idt.$func.set_handler_fn($func)}
    }
            

    //Clear IDT, in case there are any spurious interrupts
    //in there
    idt.reset();

    set_handler!(divide_by_zero);
    set_handler!(invalid_opcode);
    unsafe {
        set_handler!(double_fault).set_stack_index(0);
        set_handler!(page_fault).set_stack_index(1);
    }
    set_handler!(general_protection_fault);
    //idt[32].set_handler_fn(mem::transmute(context_switch));
    unsafe {
        println!("CONTEXT SWITCH HANDLER 0x{:x}",context_switch as u64);
        let addr = context_switch as *const ();
        //CTHULHU FTHAGN
        //idt[32].set_handler_fn(mem::transmute(addr));
        idt[32].set_handler_fn(mem::transmute_copy(&addr));
        //idt[32].set_handler_fn(timer);
    }

    //Turn interrupts on
    unsafe { asm!("sti"); }
}
//Resets the EOI flag in the apic
#[inline]
fn reset_apic() {
    unsafe { asm!("mov dword ptr [$0+0xB0],0" : : "r"(APIC_BASE) : : "intel") }
}

pub extern "x86-interrupt" fn general_protection_fault(frame: &mut InterruptStackFrame, code: u64) {
    println!("Got a general protection fault");
    println!("Error code: {}", code);
    println!("Exception stack frame: {:?}",frame);
    loop {}
}
pub extern "x86-interrupt" fn divide_by_zero(frame: &mut InterruptStackFrame)  {
    println!("Got a divide-by-zero exception");
    println!("Exception stack frame: {:?}",frame);
    loop {}
}
pub extern "x86-interrupt" fn invalid_opcode(frame: &mut InterruptStackFrame) {
    println!("Got an invalid opcode exception");
    println!("Exception stack frame: {:?}",frame);
    loop {}
}
pub extern "x86-interrupt" fn double_fault(frame: &mut InterruptStackFrame, code: u64)  {
    println!("Got a double fault");
    println!("Error code: {}", code);
    println!("Exception stack frame: {:?}",frame);
    loop {}
}

// This is our page fault handler. It will always map a page to the
// faulting address, which is enough to get us off the ground.
pub extern "x86-interrupt" fn page_fault(frame: &mut InterruptStackFrame, 
                                         code: PageFaultErrorCode) 
{

    if false && code.contains(PageFaultErrorCode::PROTECTION_VIOLATION) {
        println!("Protection violation!");
        println!("Page fault error code: {:?}",code);
        println!("Exception stack frame: {:?}",frame);
        println!("Halting");
        panic!();
    }

    //println!("GOT PAGE FAULT");
    let bad_address: u64;
    unsafe {
        asm!("mov %cr2, %rax" : "={rax}"(bad_address) : :);
    }
    
    //Keep the address aligned to a single page
    let aligned_address = x86_64::align_down(bad_address,1<<12);

    //TODO: this might be 0
    let free_page = alloc_page();

    //println!("ALLOCATING PAGE AT {:x}",free_page);

    kmap(aligned_address,free_page, 0b00001111);

    //println!("MAPPED NEW PAGE");
}
