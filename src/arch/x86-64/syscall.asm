;
;    wayless, an operating system built in Rust
;    Copyright (C) Waylon Cude 2019
;
;    This program is free software: you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation, under version 2 of the License
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program.  If not, see <http://www.gnu.org/licenses/>.
;

%include "thread.mac"
global syscall_entry 
;global kernel_syscall_return
global segments
global return_to_thread

extern pml4
extern syscall_stack_top
extern handle_raw_syscall
extern registers
extern user_cr3


; A simple spinlock to allow us to do SMP later
syscall_lock:
    dq 0


section .text
[bits 64]
syscall_entry:
    cli

    ; rcx has our next instruction to execute
    ; and r11 has rflags

    ; Acquire lock
    ;mov rsp, syscall_lock
    ;acquire:
    ;    lock bts qword [rsp],0
    ;    jc acquire
    DOLOCK syscall_lock

    ; Load kernel pml4
    ; NOTE: this trashes rsp
    ; but that's okay because rsp is a clobber anyways,
    ; it's saved into rbp beforehand
    mov rsp, pml4
    mov cr3, rsp

    ; TODO: we might need to swapgs here

    ; TODO: We might want to lock the registers structure until
    ; we get out of the syscall
    ; Save all of our registers
    SAVE_REGS

    ; These let us return values from rust by reference
    mov rcx, registers
    mov r8, user_cr3
    mov rsp, syscall_stack_top
    call handle_raw_syscall

    ; Restore the users registers
    RESTORE_REGS

    ; And then restore their cr3

    ; get the address
    ;mov rsp, user_cr3
    ; and then dereference it
    mov rsp, [rel user_cr3]
    ; and finally store it
    mov cr3, rsp

    ; TODO: shouldn't rax have a return value?
    ; Actually it looks like error messages might be
    ; returned in the message registers
    
    ; Release lock
    UNLOCK syscall_lock
    ;mov rsp, syscall_lock
    ;mov qword [rsp], 0

    sti
    o64 sysret

;Segments to get us back to kernel mode
segments:
    .cs: dw 0x8
    .ss: dw 0x10
    .ds: dw 0x10
    .es: dw 0x10
    .fs: dw 0x10
    .gs: dw 0x10

return_to_thread:
    ; Restore cr3

    mov cr3, rdi

    ; Restore the users registers
    CONTEXT_RESTORE registers


    UNLOCK syscall_lock

    sti
    ;We iret here to get back to kernel mode
    iretq

; A stack to use specifically for resuming from kernel_syscall_return
; NOTE: this probably isn't necessary, I can probably use a different stack
section .bss
    RESB 1024
switch_stack:

; vim: ft=nasm
